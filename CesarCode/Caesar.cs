﻿using System;

namespace CesarCode
{
    public class Caesar
    {
        private const string Alf = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string FormatMessage(string msg) => msg.ToUpper();

        public static string Encrypt(string msg, int count)
        {
            msg = FormatMessage(msg);
            string message = "";
            for (int i = 0; i < msg.Length; i++)
            {
                if (msg[i] == ' ')
                {
                    message += ' ';
                    continue;
                }

                int index = Alf.IndexOf(msg[i]);

                if (index < 0)
                {
                    message += ' ';
                    continue;
                }

                index = (index + count) % Alf.Length;

                message += Alf[index];
            }
            return message;
        }

        public static string Decrypt(string msg, int count)
        {
            return Encrypt(msg, Alf.Length - count);
        }
    }
}
