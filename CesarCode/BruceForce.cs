﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace CesarCode
{
    public static class BruceForce
    {
        private const string Alf = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static float[] freq_norm = {
            0.64297f,
            0.11746f,
            0.21902f,
            0.33483f,
            1.00000f,
            0.17541f,
            0.15864f,
            0.47977f,
            0.54842f,
            0.01205f,
            0.06078f,
            0.31688f,
            0.18942f,
            0.53133f,
            0.59101f,
            0.15187f,
            0.00748f,
            0.47134f,
            0.49811f,
            0.71296f,
            0.21713f,
            0.07700f,
            0.18580f,
            0.01181f,
            0.15541f,
            0.00583f
        };


        // diff 33
        public static int Force(string text)
        {
            freq_norm = freq_norm.OrderBy(x => x).ToArray();

            var s1 = GetFekList(Caesar.Decrypt(text, 7));
            var s2 = GetFekList(Caesar.Decrypt(text, 16));
            var s3 = GetFekList(Caesar.Decrypt(text, 17));
            var s4 = GetFekList(Caesar.Decrypt(text, 18));
            var s5 = GetFekList(Caesar.Decrypt(text, 19));
            var s6 = GetFekList(Caesar.Decrypt(text, 26));

            Console.WriteLine("s1: " + s1);
            Console.WriteLine("s2: " + s2);
            Console.WriteLine("s3: " + s3);
            Console.WriteLine("s4: " + s4);
            Console.WriteLine("s5: " + s5);
            Console.WriteLine("s6: " + s6);

            return 5;
        }

        private static float GetFekList(string text)
        {
            var mostList = getMost(text.ToCharArray()).OrderBy(x => x.Key);
            var sum = mostList.Sum(x => x.Value);

            List<float> fek = new List<float>();

            foreach (var f in mostList)
            {
                fek.Add(((float)f.Value) / sum);
            }

            for (int i = 0; i < fek.Count; i++)
            {
                fek[i] = freq_norm[i] + fek[i];
            }

            return fek.Sum(x => x);
        }

        public static Dictionary<char, int> getMost(char[] text)
        {
            var dictionary = new Dictionary<char, int>();
            dictionary.Add('A', 0);
            dictionary.Add('B', 0);
            dictionary.Add('C', 0);
            dictionary.Add('D', 0);
            dictionary.Add('E', 0);
            dictionary.Add('F', 0);
            dictionary.Add('G', 0);
            dictionary.Add('H', 0);
            dictionary.Add('I', 0);
            dictionary.Add('J', 0);
            dictionary.Add('K', 0);
            dictionary.Add('L', 0);
            dictionary.Add('M', 0);
            dictionary.Add('N', 0);
            dictionary.Add('O', 0);
            dictionary.Add('P', 0);
            dictionary.Add('Q', 0);
            dictionary.Add('R', 0);
            dictionary.Add('S', 0);
            dictionary.Add('T', 0);
            dictionary.Add('U', 0);
            dictionary.Add('V', 0);
            dictionary.Add('W', 0);
            dictionary.Add('X', 0);
            dictionary.Add('Y', 0);
            dictionary.Add('Z', 0);


            foreach (var c in text)
            {
                if (!Alf.Contains(c)) continue;

                if (dictionary.ContainsKey(c))
                    dictionary[c]++;
                else
                    dictionary.Add(c, 1);
            }

            return dictionary;
        }
    }
}
