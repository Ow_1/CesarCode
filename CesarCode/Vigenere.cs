﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Reflection.Metadata;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace CesarCode
{
    public class Vigenere
    {
        private const string Alf = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string Encrypt(string msg, string key) => Encode(msg, key, true);
        public static string Decrypt(string msg, string key) => Encode(msg, key, false);

        private static string Encode(string msg, string key, bool encrypt)
        {
            string message = "";
            key = Caesar.FormatMessage(key);

            //remove all spaces from key
            key = key.Replace(" ", "");

            var spaces = 0;
            for (int i = 0; i < msg.Length; i++)
            {
                var ik = (i - spaces) % key.Length;
                var k = key[ik];

                if (msg[i] == ' ')
                {
                    spaces++;
                    message += ' ';
                    continue;
                }
                message += encrypt ?
                     Caesar.Encrypt(msg[i].ToString(), Alf.IndexOf(k)):
                     Caesar.Decrypt(msg[i].ToString(), Alf.IndexOf(k));
            }
            return message;
        }
    }
}
