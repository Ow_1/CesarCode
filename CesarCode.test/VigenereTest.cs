﻿using CesarCode;
using Xunit;

namespace CeserCode.test
{
    public class VigenereTest
    {
        [Theory]
        [InlineData("Hello", "asdgg", "HWORU")]
        [InlineData("Hej med dig", "dfgjgthjley", "KJP VKW KRR")]
        [InlineData("helloworld", "test", "AIDEHAGKEH")]
        [InlineData("T EST TE STTE", "ASD", "T WVT LH SLWE")]
        public void EncryptMessege(string input, string key, string expect)
        {
            string encrypt = Vigenere.Encrypt(input, key).Trim();
            Assert.Equal(expect, encrypt);
        }

        [Theory]
        [InlineData("HWORU", "asdgg", "HELLO")]
        [InlineData("KJP VKW KRR", "dfgjgthjley", "HEJ MED DIG")]
        public void DecryptMessege(string input, string key, string expect)
        {
            string encrypt = Vigenere.Decrypt(input, key).Trim();
            Assert.Equal(expect, encrypt);
        }
    }
}
