using CesarCode;
using Xunit;

namespace CeserCode.test
{
    public class CaesarTest
    {
        [Theory]
        [InlineData("Test", "TEST")]
        public void PutsTheMsgToUpper(string input, string expect)
        {
            string back = Caesar.FormatMessage(input);
            Assert.Equal(expect, back);

        }

        [Theory]
        [InlineData("succesful unit test", 5, "XZHHJXKZQ ZSNY YJXY")]
        [InlineData("finally done!", 13, "SVANYYL QBAR")]
        public void EncryptMessege(string input, int count, string expect)
        {
            var msg = Caesar.FormatMessage(input);
            string encrypt = Caesar.Encrypt(msg, count).Trim();
            Assert.Equal(encrypt, expect);
        }

        [Theory]
        [InlineData("XZHHJXKZQ ZSNY YJXY", 5, "SUCCESFUL UNIT TEST")]
        [InlineData("SVANYYL QBAR", 13, "FINALLY DONE")]
        public void DecryptMessege(string input, int count, string expect)
        {
            string decrypt = Caesar.Decrypt(input, count).Trim();
            Assert.Equal(decrypt, expect);
        }
    }
}
